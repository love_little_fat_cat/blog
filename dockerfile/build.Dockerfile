FROM node:10.15.1-alpine
WORKDIR /build

ADD package.json /tmp/package.json
RUN cd /tmp && npm install --only=production \
    && cp -a /tmp/node_modules /build

ADD source.tar /build
RUN ./node_modules/.bin/gulp --gulpfile ./gulpfile/prod.js \
    && mv blog/html/tags.html blog \
    && mv blog/html/es.html blog \
    && mv tags.json blog \
    && mv title_path.json blog \
    && rm -r node_modules src build gulpfile* /tmp
