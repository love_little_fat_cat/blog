local word = string.sub(ngx.var.request_uri, 5)

local http = require "resty.http"
local httpc = http.new()
local res, err = httpc:request_uri(
                     "http://search.default.svc.cluster.local:8000/es/" .. word)

if not res then
  ngx.log(ngx.ERR, err)
  return
end

ngx.log(ngx.ERR, 'es ', res.status)

local json = require("cjson")
local es_list = json.decode(res.body)

local template = require "resty.template"
template.render("es.html", {es_list = es_list})
