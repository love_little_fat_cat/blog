local _M = {}
local blacklist_ips = {}
local whitelist_ips = {
  "112.97.0.0/16", "144.202.84.187", "149.248.38.34", "218.17.0.0/16", "120.229.0.0/16"
}

function _M.get_blacklist_ips() return blacklist_ips end

function _M.get_whitelist_ips() return whitelist_ips end

return _M
