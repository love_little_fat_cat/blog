local server = require "resty.websocket.server"

local wb, err = server:new{
  timeout = 5000, -- in milliseconds
  max_payload_len = 65535
}
if not wb then
  ngx.log(ngx.ERR, "failed to new websocket: ", err)
  return ngx.exit(444)
end

while true do
  local data, typ, err = wb:recv_frame()

  if not data then
    if not string.find(err, "timeout", 1, true) then
      ngx.log(ngx.ERR, "failed to receive a frame: ", err)
      return ngx.exit(444)
    else
      -- ngx.log(ngx.ERR, "receive frame timeout: ", err)
      goto continue
    end
  end

  if typ == "close" then
    -- for typ "close", err contains the status code
    local code = err

    -- send a close frame back:

    local bytes, err = wb:send_close(1000, "enough, enough!")
    if not bytes then
      ngx.log(ngx.ERR, "failed to send the close frame: ", err)
      return
    end
    ngx.log(ngx.ERR, "closing with status code ", code, " and message ", data)
    return
  end

  local f = io.popen("stat -c %Y /root/blog/title_path.json")
  local last_modified = f:read()
  if typ == "ping" then
    -- send a pong frame back:
    local bytes, err = wb:send_pong(last_modified)
    if not bytes then
      ngx.log(ngx.ERR, "failed to send frame: ", err)
      return
    end
  elseif typ == "pong" then
    -- just discard the incoming pong frame

  else
    ngx.log(ngx.ERR, "received a frame of type ", typ, " and payload ", data)
    wb:send_text(last_modified)
  end

  ::continue::
end
