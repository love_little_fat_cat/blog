<!-- build:title -->Wireshark查看GRPC流量<!-- /build:title -->
<!-- build:tags -->HTTP2,grpc<!-- /build:tags -->
<!-- build:content -->
## Wireshark查看GRPC流量

### linux

直接用wireshark查看，wireshark会把grpc流量当作tcp流量

![](../images/wireshark-grpc-no-setting.png)

需要**设置**

![](../images/wireshark-grpc-setting.png)

设置后就能看到grpc流量了

![](../images/wireshark-grpc.png)

### mac

mac环境一样的,需要**设置**,设置后就能看到grpc流量了

![](../images/wireshark-grpc1.png)

<!-- /build:content -->
