<!-- build:title -->openresty设置访问白名单<!-- /build:title -->
<!-- build:tags -->openresty,nginx<!-- /build:tags -->
<!-- build:content -->
## alpine编译libcidr

可以使用[lua-resty-iputils](https://github.com/hamishforbes/lua-resty-iputils),但是不支持`ipv6`.[lua-resty-iputils](https://github.com/hamishforbes/lua-resty-iputils)的作者推荐[lua-libcidr-ffi](https://github.com/GUI/lua-libcidr-ffi),以支持`ipv6`.`lua-libcidr-ffi`需要`libcidr`,我使用`alpine`作为基础镜像.搜了一下，貌似`alpine`上没有直接可以使用的`libcidr`,就自己编译个吧

```bash
    ...
    && curl -fSL http://www.over-yonder.net/~fullermd/projects/libcidr/libcidr-1.2.3.tar.xz -o /tmp/libcidr-1.2.3.tar.xz \
    && xz -d /tmp/libcidr-1.2.3.tar.xz && tar -xvf /tmp/libcidr-1.2.3.tar \
    && cd libcidr-1.2.3/src && make && mv libcidr.so.0 /usr/local/openresty/lualib/libcidr.so \
    ...
```

- `alpine`需要安装`xz`,`coreutils`.其中`xz`用来解压,`libcidr`编译的时候会用到`coreutils`里面的`tsort`
- `make`后不用`make DESTDIR=/your_path install`.执行`make DESTDIR=/your_path install`的话会报错.其实`make`执行后，就已经生成so文件了,只不过叫`libcidr.so.0`.把这个文件移动到`openresty`可以解析的目录就可以了

## 修改libcidr-ffi.lua

只是编译了`libcidr`的话，运行会报无法找到模块.所以还需要修改`libcidr-ffi.lua`文件,找到里面的

```lua
local cidr = ffi.load("cidr")
```

改为

```lua
local cidr = ffi.load("/usr/local/openresty/lualib/libcidr.so")
```

路径就是上面编译后移动到的路径   
构建镜像的时候，记得把修改后的`libcidr-ffi.lua`加到镜像里

## 使用lua-libcidr-ffi

whitelist_init.lua

```lua
whitelist_ips = {
    "103.21.244.0/22",
}
```

whitelist_access.lua

```lua
local cidr = require("resty.libcidr-ffi")
local flag=false
for i, v in ipairs(whitelist_ips) do
  --proxy_protocol_addr在白名单里
  if cidr.contains(cidr.from_str(v), cidr.from_str(ngx.var.proxy_protocol_addr)) then
    ngx.log(ngx.ERR,ngx.var.proxy_protocol_addr,' match ',v)
    flag=true
    break
  end
end

if not flag then
  return ngx.exit(ngx.HTTP_FORBIDDEN)
end
```

nginx.conf

```nginx
http{
    init_by_lua_file whitelist_init.lua;

    server{
        listen 4430;
        location / {
            access_by_lua_file whitelist_access.lua;
        }
    }
}
```

## nginx allow/deny directives

使用nginx的[allow/deny指令](http://nginx.org/en/docs/stream/ngx_stream_access_module.html)也可以实现，不过要注意的是，allow/deny指令默认对`remote_addr`过滤，如果nginx前面有反向代理，`remote_addr`将会是127.0.0.1,使指令不起作用.所以需要使用`ngx_http_realip_module`,修改allow/deny指令的过滤对象   
`ngx_http_realip_module`需要编译的时候添加

```bash
--with-http_realip_module
```

allowed-ip.conf

```nginx
allow 1.1.1.0/22;
...
```

nginx.conf

```nginx
real_ip_header proxy_protocol;
set_real_ip_from 127.0.0.1;

include /root/allowed-ip.conf;
deny all;
```

- real_ip_header

> Defines the request header field whose value will be used to replace the client address.   
Syntax:real_ip_header field | X-Real-IP | X-Forwarded-For | proxy_protocol;

- set_real_ip_from

> Defines trusted addresses that are known to send correct replacement addresses

set_real_ip_from必须要设置，否则real_ip_header不起作用

---
不定期更新
<!-- /build:content -->
