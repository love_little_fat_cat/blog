<!-- build:title -->fedora使用Wireshark调试HTTP2流量<!-- /build:title -->
<!-- build:tags -->HTTP2,linux<!-- /build:tags -->
<!-- build:content -->
本文和ququ大神的[使用 Wireshark 调试 HTTP/2 流量](https://imququ.com/post/http2-traffic-in-wireshark.html)几乎一样，水了一篇，好开心

配置SSLKEYLOGFILE

```bash
mkdir ~/dev/tls && touch ~/dev/tls/sslkeylog.log
echo "\nexport SSLKEYLOGFILE=~/dev/tls/sslkeylog.log" >> ~/.zshrc && source ~/.zshrc
```

配置wireshark

![](../images/wireshark-http2.png)

命令行启动firefox

```bash
firefox
```

不用root用户打开wireshark,然后点击Capture下面的interface.普通用户下sudo也可以

```bash
sudo tcpdump -i wlp6s0 -w ~/test.cap
```

![](../images/wireshark-http2-show.png)
<!-- /build:content -->
