<!-- build:title -->openresty stream-lua-nginx-module尝鲜<!-- /build:title -->
<!-- build:tags -->openresty<!-- /build:tags -->
<!-- build:content -->
最新的openresty(1.13.6.1)，[stream-lua-nginx-module](https://github.com/openresty/stream-lua-nginx-module)默认用的是v0.0.3.而`stream-lua-nginx-module`的作者上月加了一些特性,于是赶紧试一下

## 编译安装stream-lua-nginx-module v0.0.5

可以参见我的[Dockerfile](https://gitlab.com/love_little_fat_cat/docker-openresty/blob/master/Dockerfile)和[commit](https://gitlab.com/love_little_fat_cat/docker-openresty/commit/c43281add3fd198544ca06c76e29129673b60b0c)

说明

- 需要在编译参数中加上`--without-stream_lua_module`,禁止编译默认的stream-lua-nginx-module版本。docker构建时删除，替换stream-lua-nginx-module无效
- `--add-module=/tmp/stream-lua-nginx-module-0.0.5`,使用nginx动态编译模块

## lua_add_variable

新的stream-lua-nginx-module添加了[lua_add_variable](https://github.com/openresty/stream-lua-nginx-module#lua_add_variable)指令,使得在stream block中,**nginx的内置绑定变量可以通过ngx.var获得**

> Add variable $var to the stream subsystem and makes it changeable. If $var already exists, this directive will do nothing.

### ssl preread

nginx的[ngx_stream_ssl_preread_module](http://nginx.org/en/docs/stream/ngx_stream_ssl_preread_module.html)文档有个根据SNI路由的例子,在map中使用了`$ssl_preread_server_name`。现在直接`ngx.var.ssl_preread_server_name`就行了

```nginx
stream{
    upstream proxy_backend{
        server 127.0.0.1:9527;
        balancer_by_lua_block{
            local ssl_preread_server_name=ngx.var.ssl_preread_server_name
            ...
        }
    }

    server {
        listen 443;
        ssl_preread on;
        proxy_pass proxy_backend;
    }
}
```

### dynamic route

`balancer_by_lua_*`能实现动态路由，`lua_add_variable`结合[preread_by_lua__*](https://github.com/openresty/stream-lua-nginx-module#preread_by_lua_block)一样可以做到

```nginx
stream{
    lua_add_variable $route;
    server {
        listen 443;
        #ssl_preread on;
        preread_by_lua_block{
           ngx.var.route='127.0.0.1:9527';
        }
        proxy_pass $route;
    }
}
```

注意使用了`preread_by_lua_*`就不要设置`ssl_preread on;`了,因为nginx SNI解析成功后,会跳过`preread阶段`的剩余部分.而`preread_by_lua_*`总是在preread阶段的最后执行

> The preread_by_lua_block code will always run at the end of the preread processing phase

可以设置`preread_by_lua_no_postpone:on;`改变`preread_by_lua_*`在`preread阶段`的执行时间

> Controls whether or not to disable postponing preread_by_lua* directives to run at the end of the preread processing phase

## 无法获取$ssl_preread_server_name

在`1.13.6.1`中,如果使用了`preread_by_lua_*`,则无法通过lua获取$ssl_preread_server_name

```nginx
stream{
    upstream proxy_backend{
        server 127.0.0.1:9527;
        balancer_by_lua_block{
            --ssl_preread_server_name:nil
            ngx.log(ngx.ERR,ngx.var.ssl_preread_server_name)
        }
    }
    server {
        listen 443;
        proxy_pass proxy_backend;
        preread_by_lua_block{
            --ssl_preread_server_name:nil
            ngx.log(ngx.ERR,ngx.var.ssl_preread_server_name)
        }
    }
}
```

这个问题在`1.13.6.2`中已修复

> feature: added patches to the nginx core to make sure ngx_stream_ssl_preread_module will not skip the rest of the preread phase when SNI server name parsing was successful. thanks Datong Sun for the patch.

这样修改就好了

```nginx
stream{
    upstream proxy_backend{
        server 127.0.0.1:9527;
        balancer_by_lua_block{
            --it works!
            ngx.log(ngx.ERR,ngx.var.ssl_preread_server_name)
        }
    }
    server {
        listen 443;
        ssl_preread on;
        proxy_pass proxy_backend;
        preread_by_lua_block{
            --it works!
            ngx.log(ngx.ERR,ngx.var.ssl_preread_server_name)
        }
    }
}
```

- 这里必须要设置`ssl_preread on;`
- 设置的`ssl_preread on;`不会像上面说的,与`preread_by_lua_*`有冲突,因为在`1.13.6.2`中,nginx SNI解析成功后,不会跳过`preread阶段`的剩余部分,`preread_by_lua_*`会在`preread阶段`最后执行
- 不要设置`preread_by_lua_no_postpone on;`,因为如果设置了，`preread_by_lua_*`不会在`preread阶段`最后执行，这时nginx SNI还没解析

---
不定期更新

<!-- /build:content -->
