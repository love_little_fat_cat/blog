<!-- build:title -->gitlab CI/CD优化<!-- /build:title -->
<!-- build:tags -->gitlab-CI-CD,docker<!-- /build:tags -->
<!-- build:content -->
我的[配置](/gitlab-ci-cd-config.html)

## docker image cache

### docker-in-docker

[docker-in-docker(dind)](https://hub.docker.com/_/docker/)顾名思义，就是在docker里面运行docker.由于要在docker里控制里面的docker,所以要开启[privileged mode](https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities)

`/etc/gitlab-runner/config.toml`配置

```
[[runners]]
  ...
  executor = "docker"
  [runners.docker]
    ...
    privileged = true
    volumes = ["/cache"]
  [runners.cache]
```

另外，`.gitlab-ci.yml`需要配置[services](https://gitlab.com/help/ci/yaml/README#image-and-services)为dind

```yaml
image: docker:stable
services:
  - docker:dind
```

dind会保证每次创建的`job`都是一个干净的环境，之前执行`job`所build的镜像都不会保存.比如，我的[配置](/gitlab-ci-cd-config.html)中，[build stage](/gitlab-ci-cd-config.html)里的**$CACHE_IMAGE_URL,$BUILD_IMAGE_URL,node:8.11.2-alpine**都不会缓存在docker executor,当然，更不可能在gitlab runner的host上了。下次有job执行时，只能重新从registry拉取,无法利用docker缓存   
这里能优化的地方，只有创建一个tag为cache的镜像，在构建docker镜像时,先拉取tag为cache的镜像，在`build`的时候，使用[--cache-from](https://docs.docker.com/engine/reference/commandline/build/#options)参数,像这样

```bash
docker pull your_image:cache || true
docker build --cache-from your_image:cache -t your_image:your_tag -t your_image:cache .
docker push your_image:cache
docker push your_image:your_tag
```

### Docker socket binding

如果觉得`docker in docker`每次都需要pull,浪费时间，可以使用`docker socket bind`.

`/etc/gitlab-runner/config.toml`配置

```
[[runners]]
  ...
  executor = "docker"
  [runners.docker]
    ...
    privileged = false
    volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock"]
  [runners.cache]
```

`.gitlab-ci.yml`不需要配置services

`docker socket bind`和gitlab runner host共用docker daemon,所以build的镜像都会缓存在host,这样job执行时不用每次都去registry pull

## nodejs module cache

我的博客在gitlab ci构建的时候,需要nodejs编译.而编译需要一些nodejs package,我们当然不想每次构建的时候,都`npm install`重新下载包.这就需要将`node_modules目录`缓存起来.    

### shell executor

如果[gitlab runner](https://docs.gitlab.com/runner/) executors设置的是[shell](https://docs.gitlab.com/runner/executors/shell.html)

> The Shell executor is a simple executor that allows you to execute builds locally to the machine that the Runner is installed

构建的时候，相当于在装有`gitlab runner`的机器上执行[.gitlab-ci.yml](https://gitlab.com/help/ci/yaml/README)的`script`.`npm install`后,node_modules目录会在`gitlab runner`的工作目录里.使用gitlab CI配置里的[cache](https://gitlab.com/help/ci/yaml/README#cache),就可以在不同的`job`里使用缓存的node_modules目录.

### docker executor

[docker executor](https://docs.gitlab.com/runner/executors/docker.html)的话，就不能使用缓存在gitlab runner机器上的node_modules目录,因为每次构建都会创建新的容器

> The Docker executs when used with GitLab CI, connects to Docker Engine and runs each build in a separate and isolated container

怎么办？好在docker layer是可以缓存的

> After building the image, all layers are in the Docker cache.   
In Docker 1.10 and higher, only RUN, COPY, and ADD instructions create layers. Other instructions create temporary intermediate images, and no longer directly increase the size of the build.

将`npm install`和后面的命令分开，不用&&拼接命令,对`npm install`单独一行`RUN`指令。

```bash
FROM node:alpine
WORKDIR /blog

ADD package.json /tmp/package.json
RUN cd /tmp && npm install --only=production \
    && cp -a /tmp/node_modules /blog

ADD . /blog
RUN ./node_modules/.bin/gulp --gulpfile gulpfile-prod.js \
    && mv html/index.html . \
    && rm -r node_modules src package* gulpfile* /tmp
```

这样在构建的时候，node_modules目录会成为一层`layer`,被缓存起来.

![图片标题](../images/gitlab-ci-cache.png)

注意如果用的是`dind`,需要和上面**docker-in-docker executor**那的`--cache-from`那段代码一起使用才能缓存`node_modules目录`,因为`dind`不会在docker executor缓存镜像

## multi-stage builds

[multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/)可以让我们在构建docker镜像时，使用不同的基础镜像   
比如我的博客需要nodejs环境编译，但是部署，上线不需要nodejs环境，需要`openresty`.Dockerfile可以这样写

```bash
FROM node:alpine as node
#编译博客
...

FROM openresty:alpine
COPY --from=node src dst 
```

[copy指令](https://docs.docker.com/engine/reference/builder/#copy)将nodejs编译好的目录拷贝到`openresty`里

## OverlayFS driver

如果使用的是docker executor的`docker in docker mode`,storage-driver用的是vfs,可以使用更好的storage-driver，[参见](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-the-overlayfs-driver)

## apk add --virtual

`alpine`安装的时候,可以将部署，上线时不需要的包安装到一个目录，最后再删除这个目录

```bash
apk add --no-cache --virtual .build-deps package...
apk del .build-deps
```

## 参考

> [Building Docker images with GitLab CI/CD](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)   
[Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

---
不定期更新
<!-- /build:content -->
