<!-- build:title -->vim go开发环境<!-- /build:title -->
<!-- build:tags -->vim,go<!-- /build:tags -->
<!-- build:content -->
主要使用[vim-go](https://github.com/fatih/vim-go)插件,教程[vim-go-tutorial](https://github.com/fatih/vim-go-tutorial)   

## installation

vim-go需要安装一些go执行文件，所以安装时最好

1. 先打开代理

```bash
export all_proxy=socks5://127.0.0.1:2080
```

使用go module也一样

在~/.gitconfig里配置http proxy不起作用,因为go命令的代理是基于http的

```
[http]
    proxy = socks5://127.0.0.1:2080
```

2. 配置插件

```vim
call vundle#begin()
...
Bundle 'fatih/vim-go'
...
call vundle#end()
```

3. 在command mode下执行`:BundleInstall`

## update go binary

更新go执行文件也一样，最好

1. 先打开代理
2. 在command mode下执行`:GoUpdateBinaries`

## code completion

代码补全用[YouCompleteMe](https://github.com/Valloric/YouCompleteMe)就好,安装时要加上`--go-completer`

```bash
./install.py --go-completer
```

另外还需要[gocode](https://github.com/nsf/gocode)或[godef](https://github.com/Manishearth/godef)   

> a combination of Gocode and Godef semantic engines for Go

安装一个即可

## go to declaration

可以使用

- YouCompleteMe的`:YcmCompleter GoToDeclaration`

```vim
Bundle 'Valloric/YouCompleteMe'
nnoremap <leader>d :YcmCompleter GoToDeclaration<CR>
```

- 或vim-go的`:GoDef`

```vim
Bundle 'fatih/vim-go'
nnoremap <leader>d :GoDef<CR>
```

## declaration stack

通常进入(step in)一个function看代码，还会继续进入其它function,这过程中会移动光标，如果使用`Ctrl+o`(返回上一个位置),会有一些位置不是function进入点.   
可以使用`Ctrl+t`替代`Ctrl+o`,这样每次跳转到的上一个位置都是function的进入点   
如果要查看所有的function进入点,调用`:GoDefStack`

```vim
autocmd FileType go nmap <leader>s :GoDefStack<CR>
```

注意使用declaration stack的前提是上一节`go to declaration`使用的是vim-go的`:GoDef`

## show usage

**显示被何处调用**这个功能,YouCompleteMe没有提供对go的支持,但是vim-go有支持

```vim
Bundle 'Valloric/YouCompleteMe'
autocmd FileType python nnoremap <leader>r :YcmCompleter GoToReferences<CR>

Bundle 'fatih/vim-go'
autocmd FileType go nmap <leader>r :GoReferrers<CR>
```

## import

[自动导入包](https://github.com/fatih/vim-go-tutorial#imports)这个功能挺有用的

```vim
nmap <leader>i :GoImports<CR>
let g:go_fmt_command = "goimports"
```

自动导入会使用`go_fmt_command`配置的命令，默认是gofmt.可以给命令添加参数

```vim
let g:go_fmt_options = {
    \ 'gofmt': '-s',
    \ 'goimports': '-local your_string',
    \ }
```

## linter

`:GoMetaLinter`默认同时执行go vet,golint,errcheck,可以指定执行它们中的部分

```vim
let g:go_metalinter_enabled = ['vet', 'errcheck']
```

总是在命令模式输入`:GoMetaLinter`不方便，可以配置为在保存的时候检测,这时也可以指定linter

```vim
let g:go_metalinter_autosave = 1
let g:go_metalinter_autosave_enabled = ['vet', 'errcheck']
```

## jump between functions

vim-go重写了快捷键`[[`和`]]`,可以跳转到上一个(下一个)function

## resolution

经常需要看看调用的function的参数和返回,通常的操作是进入(step in)这个function看，但是这样会跳转，看完后需要`Ctrl+o`回到之前位置   
使用`:GoInfo`就能不用step in就知道调用的function的参数和返回

```vim
autocmd FileType go nmap <leader>i :GoInfo<CR>
```

和上面一样，总是在命令模式输入`:GoInfo`不方便，可以配置为自动显示

```vim
let g:go_auto_type_info = 1
set updatetime=500
```

`updatetime`表示在指定时间后，显示光标所在function的信息

## work with IndentLine

### tab indent

自动导入功能最终会调用gofmt或goimports,它们都是`tab缩进`,效果是缩进8个空格。这时配置vim就可以有[IndentLine](https://github.com/Yggdroot/indentLine)的效果

```vim
set tabstop=4
set shiftwidth=4
set expandtab

set list
"最后的\后面有1个空格
set listchars=tab:\|\ 
```

tab还是转为4个space

![](../images/vim-go-tab-indent.png)

### space indent

如果不想用自动导入功能，想space缩进4个空格,[IndentLine](https://github.com/Yggdroot/indentLine)的线会消失

![](../images/vim-go-space-indent.png)

需要设置

```vim
autocmd FileType go setlocal expandtab
```

参考[Seems IndentLine has conflicts with vim-go](https://github.com/Yggdroot/indentLine/issues/210)

---
本文只是抛砖引玉，实际上，vim-go以及它所使用的go工具还有很多用法，值得去发掘，以使得开发效率更高，让vim用着更顺手，提升编程体验

不定期更新
<!-- /build:content -->
