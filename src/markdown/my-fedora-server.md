<!-- build:title -->fedora server设置<!-- /build:title -->
<!-- build:tags -->linux<!-- /build:tags -->
<!-- build:content -->
```bash
dnf update
dnf install vim git util-linux-user htop zsh wget tcpdump ack bind-utils rsyslog ctags httpd-tools
```

## tcp bbr

```bash
modprobe tcp_bbr 
echo "tcp_bbr" >> /etc/modules-load.d/modules.conf 
echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf 
sysctl -p 
```

`lsmod | grep bbr`出现bbr,则修改成功

## 禁用selinux

`vim /etc/selinux/config`

```bash
SELINUX=disabled
```

重启

## 防火墙使用iptables

```bash
dnf remove firewalld
dnf install iptables-services

systemctl enable iptables ip6tables
systemctl restart iptables ip6tables
```

[iptables限制频率并自定义黑名单](/iptables-blacklist.html)

## 创建用户

```bash
useradd -g users hehe
passwd hehe
```

## zsh

```bash
dnf install zsh git
chsh -s $(which zsh)

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search

plugins=(zsh-syntax-highlighting zsh-autosuggestions history-substring-search)
```

## vim

- vundle

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

- 配置参见[我的vim配置](/vim_conf.html)

## tmux

```bash
dnf install tmux
```

配置参见[我的tmux配置](/tmux_conf.html)

## docker

```bash
wget -O ~/docker-ce-17.12.1.ce-1.fc27.x86_64.rpm https://download.docker.com/linux/fedora/27/x86_64/stable/Packages/docker-ce-17.12.1.ce-1.fc27.x86_64.rpm
dnf install -y docker-ce-17.12.1.ce-1.fc27.x86_64.rpm
```

/etc/docker/daemon.json

```json
{
  "metrics-addr" : "127.0.0.1:9323",
  "experimental" : true
}
```

## docker-compose

```bash
curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

## nginx

```bash
dnf install gcc gcc-c++
wget https://www.zlib.net/zlib-1.2.11.tar.gz && tar xzvf zlib-1.2.11.tar.gz
wget https://www.openssl.org/source/openssl-1.1.0g.tar.gz && tar xzvf openssl-1.1.0g.tar.gz
wget https://ftp.pcre.org/pub/pcre/pcre-8.41.tar.gz && tar xzvf pcre-8.41.tar.gz
wget http://nginx.org/download/nginx-1.13.9.tar.gz && tar xzvf nginx-1.13.9.tar.gz

./configure --sbin-path=/usr/sbin/nginx --with-http_ssl_module --with-http_v2_module --with-stream --with-stream_ssl_preread_module --with-openssl=/root/openssl-1.1.0g --with-zlib=/root/zlib-1.2.11 --with-pcre=/root/pcre-8.41
make && make install
```

## gitlab runner

```bash
wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
useradd --create-home gitlab-runner
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start
gitlab-runner register
```

## pip3

```bash
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
```

## ansible

```bash
pip3 install ansible
```

`~/.ansible.cfg`

```
[defaults]
remote_tmp = /tmp
```

## https证书

```bash
dnf install socat
curl https://get.acme.sh | sh

#生成ecc证书
~/.acme.sh/acme.sh --issue --standalone -d theviper24.today -d www.theviper24.today -k ec-256
```

注意执行前，清除iptables规则，开放相应端口

## influxdb

```bash
wget https://dl.influxdata.com/influxdb/releases/influxdb-1.5.3_linux_amd64.tar.gz
tar xvfz influxdb-1.5.3_linux_amd64.tar.gz

./influxdb/usr/bin/influxd -config ./influxdb/influxdb.conf

influx -precision rfc3339
CREATE DATABASE prometheus
auth

influx -precision rfc3339 -username admin -password admin
```

## prometheus

```bash
wget https://github.com/prometheus/prometheus/releases/download/v2.2.1/prometheus-2.2.1.linux-amd64.tar.gz
tar xzvf prometheus-2.2.1.linux-amd64.tar.gz

./prometheus/prometheus --config.file=./prometheus/prometheus.yml --web.listen-address=:9090 --web.route-prefix=/ --web.external-url=http://127.0.0.1:9090/prome/
```

```yaml
remote_write:
  - url: "http://localhost:8086/api/v1/prom/write?u=&p=&db=prometheus"
remote_read:
  - url: "http://localhost:8086/api/v1/prom/read?u=&p=&db=prometheus"
```

## grafana

```bash
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-5.1.3.linux-x64.tar.gz
tar -zxvf grafana-5.1.3.linux-x64.tar.gz

./grafana/bin/grafana-server -config=./grafana/conf/defaults.ini -homepath=./grafana web
```

root_url = %(protocol)s://%(domain)s/grafana/

---
不定期更新
<!-- /build:content -->
