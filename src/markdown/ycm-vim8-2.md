<!-- build:title -->ubuntu 18.04 编译vim 8.2<!-- /build:title -->
<!-- build:tags -->vim<!-- /build:tags -->
<!-- build:content -->
## ubuntu 18.04 编译vim 8.2

因为YouCompleteMe最新版本需要Vim v8.1.2269+

- 安装依赖

```bash
apt install libncurses5-dev
```

- 编译

> 1. Vim v8.1.2269 huge build, compiled with Python 3.6 support (aka vim-nox in Ubuntu 20.04 LTS)   
2. Python 3.6 runtime, compiled with --enable-shared (or --enable-framework)

[Requirements](https://github.com/ycm-core/YouCompleteMe#requirements)

对于第2点,可以不用指定--enable-shared.设置好python3相关编译参数就行了

```bash
git clone https://github.com/vim/vim.git
cd vim/src
./configure --with-features=huge --enable-python3interp=yes --with-python3-config-dir=/usr/lib/python3.6/config-3.6m-x86_64-linux-gnu --prefix=/usr/local
sudo make install
```

<!-- /build:content -->
