<!-- build:title -->iptables限制频率并自定义黑名单<!-- /build:title -->
<!-- build:tags -->iptables,kubernetes,docker<!-- /build:tags -->
<!-- build:content -->
下面的iptables规则都是修改的`/etc/sysconfig/iptables`,需要先安装`iptables-services`

```bash
dnf install -y iptables-services
```

## 定义规则

- 黑名单规则

```bash
-N black
-A black -m recent --set --name blacklist -j DROP
-A INPUT -p tcp -m state --state NEW -m recent --update --name blacklist --reap --seconds 2592000 -j DROP
```

最后一行表示，所有试图建立连接的tcp连接,如果在`blacklist`里，且当前时间-`blacklist`记录的时间<1个月,就会被丢弃

- 频率限制规则

```bash
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -m recent --update --name timer --reap --seconds 60 --hitcount 200 -j black
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -m recent --set --name timer -j ACCEPT
```

第一行表示，所有试图建立连接的tcp连接,如果在60秒内，超过200次尝试建立连接，则会被加到`black`链中.   
而由前面定义的黑名单可以看到，所有`black`过来的连接，都会被记录到`blacklist`,进而被封1个月

## 从黑名单移除ip

```bash
echo -x.x.x.x>/proc/net/xt_recent/blacklist
```

## 修改recent模块记录的ip数量

recent模块默认只会记录`1024`个ip,也就是说，黑名单最多只能存放`1024`个ip,这显然不能满足要求,需要修改配置

- 关闭iptables
- 新建`xt_recent.conf`

```bash
vim /etc/modprobe.d/xt_recent.conf
```

- 在`xt_recent.conf`文件写入

```bash
options xt_recent ip_list_tot=10240
```

- 重新加载模块

```bash
modprobe -r xt_recent
```

- 启动iptables

查看修改

```bash
cat /sys/module/xt_recent/parameters/ip_list_tot
```

## 注意事项

注意顺序！黑名单规则一定要在频率限制规则前面，你懂的！   
另外，如果要防止端口扫描

```bash
-A INPUT -p tcp -m state --state NEW -m recent --update --name timer --reap --seconds 31536000 --hitcount 1 -j black 
-A INPUT -p tcp -m state --state NEW -m recent --set --name timer -j ACCEPT
```

一定要将你的ssh登录端口放到最前面，你懂的！

```bash
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
```

## docker与host通信

当docker访问host端口时,需要将docker创建的`网卡加入iptables白名单`

比如，现在`docker network create hehe`创建hehe,docker compose中配置网络,使用hehe

```yaml
version: '3'
services:
  redis:
    container_name: "redis"
    image: "redis:alpine"
    networks:
      - hehe
    restart: always
networks:
  hehe:
    external: true
```

`docker network ls`,找到

```bash
NETWORK ID          NAME                DRIVER              SCOPE
d91061d312a1        hehe                bridge              local
```

在iptable添加

```bash
-A INPUT -i br-d91061d312a1 -j ACCEPT
```

## kubernetes

`kubernetes`会自动在iptables添加很多规则，如果像上面，直接编辑`/etc/sysconfig/iptables`,再`systemctl restart iptables`使其生效的话，会丢失kubernetes添加的规则.所以需要

1. 在自己添加规则前，执行`iptables-save > rule`,将当前kubernetes添加的规则保存下来
2. 编辑rule文件
3. 执行`iptables-restore -v < rule`使其生效

编辑rule文件的话，找到`filter表`,将原来的`/etc/sysconfig/iptables`复制过来

```
*filter
:INPUT ACCEPT [0:101868]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [475:2336784]
:DOCKER - [0:0]
:DOCKER-ISOLATION - [0:0]
:DOCKER-USER - [0:0]
:KUBE-EXTERNAL-SERVICES - [0:0]
:KUBE-FIREWALL - [0:0]
:KUBE-FORWARD - [0:0]
:KUBE-SERVICES - [0:0]
:black - [0:0]
-A INPUT -m conntrack --ctstate NEW -m comment --comment "kubernetes externally-visible service portals" -j KUBE-EXTERNAL-SERVICES

#edit rule here
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT

-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
...

#-N black
-A black -m recent --set --name blacklist -j DROP
-A INPUT -p tcp -m state --state NEW -m recent --update --seconds 31104000 --reap --name blacklist --mask 255.255.255.255 --rsource -j DROP

-A INPUT -p tcp -m state --state NEW -m recent --update --seconds 31104000 --reap --hitcount 1 --name timer --mask 255.255.255.255 --rsource -j black
-A INPUT -p tcp -m state --state NEW -m recent --set --name timer --mask 255.255.255.255 --rsource -j ACCEPT

-A INPUT -p tcp -j DROP
...
COMMIT
```

注意

- `:black - [0:0]`和`-N black`有一个就可以了，否则无法执行
- 需要对`udp协议`放行(第30行),否则kubernetes dns无效,可以执行`kubectl exec your_pod nslookup kubernetes`,如果有类似输出则kubernetes dns有效

```
Name:      kubernetes
Address 1: 10.96.0.1 kubernetes.default.svc.cluster.local
```

- 如果docker或服务器重启(注意先关闭firewalld)，需要重新执行上面操作

另外,执行helm命令，如`helm ls`时，如果很久没反应，最后输出

```
Error: Get https://10.96.0.1:443/api/v1/namespaces/kube-system/configmaps?labelSelector=OWNER%!D(MISSING)TILLER: dial tcp 10.96.0.1:443: connect: no route to host
```

一般也是iptables的问题

---
不定期更新
<!-- /build:content -->
