<!-- build:title -->命令行模糊搜索神器-fzf<!-- /build:title -->
<!-- build:tags -->vim<!-- /build:tags -->
<!-- build:content -->
## command line fuzzy finder

### Search syntax

[语法](https://github.com/junegunn/fzf#search-syntax)和正则表达式类似:`^`表示开头,`$`表示最后，`!`表示取反.不同点是:`'`开头表示精确匹配,还支持`|(或)`

### multi-select mode

fzf支持多个选中.打开界面，移动到目标行，`TAB`选中即可

### trigger fzf

打开界面的方式:

- COMMAND [DIRECTORY/][FUZZY_PATTERN]**<TAB>
- CTRL+T

### file preview

```bash
export FZF_DEFAULT_OPTS='--border --preview "[[ $(file --mime {}) =~ binary ]] &&
                                            echo {} is a binary file ||(bat --style=numbers --color=always --theme=TwoDark {}) 2> /dev/null | head -1000"'
```

如果文件不是binary，[bat](https://github.com/sharkdp/bat)读取文件，`head`输出前1000行

如果想默认关闭preview,`FZF_DEFAULT_OPTS`后面加上

```bash
export FZF_DEFAULT_OPTS='...--preview-window "hidden"' 
```

### preview bind key

```bash
export FZF_DEFAULT_OPTS='--border --preview "[[ $(file --mime {}) =~ binary ]] &&
                                            echo {} is a binary file ||(bat --style=numbers --color=always --theme=TwoDark {}) 2> /dev/null | head -1000" 
                                            --bind "?:toggle-preview,up:preview-up,down:preview-down" 
                                            '
```

通过`--bind`参数

- 绑定快捷键`?`,触发&关闭preview
- 绑定上,下箭头，上下移动preview的内容

### directory preview

`ALT-C`打开目录选择界面

```bash
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200' --bind '?:toggle-preview,up:preview-up,down:preview-down'"
```

和上面file preview绑定一样，通过`--bind`参数

- 绑定快捷键`?`,触发&关闭preview
- 绑定上,下箭头，上下移动preview的内容

### ignore file

fzf默认用`find`查找，可以替换为其他工具，如[fd](https://github.com/sharkdp/fd),以避免不想看到的文件出现在fzf结果中

```bash
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
```

### 演示

<video controls preload='none'>
    <source src="https://theviper.xyz/videos/fzf.webm" type="video/webm">
    Your browser does not support webm format video.
</video>

## fzf.vim

文件快速打开和全文搜索之前用的是

```vim
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mileszs/ack.vim'
map <leader>a :Ack!<space>
```

现在这两个可以换成

```vim
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'
map <leader>a :Ag!<space>
nnoremap <C-p> :Files<Cr>
```

[ag](https://github.com/ggreer/the_silver_searcher)

> A code-searching tool similar to ack, but faster

`:Files`类似于`ctrlp`.   
由于调用了`fzf命令`,会用到上面的配置,所以也可以通过上面绑定的快捷键preview

### ag preview

`ag`全文搜索的结果也可以preview

```vim
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>,
  \                 <bang>0 ? fzf#vim#with_preview('up:60%')
  \                         : fzf#vim#with_preview('right:50%:hidden', '?'),
  \                 <bang>0)
```

和上面fzf preview绑定一样

- 绑定快捷键`?`,触发&关闭preview
- 绑定上,下箭头，上下移动preview的内容

### 效果

<video controls preload='none'>
    <source src="https://theviper.xyz/videos/fzf-vim.webm" type="video/webm">
    Your browser does not support webm format video.
</video>

---
不定期更新
<!-- /build:content -->
