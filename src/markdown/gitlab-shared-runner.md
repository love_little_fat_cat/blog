<!-- build:title -->使用gitlab shared runner<!-- /build:title -->
<!-- build:tags -->gitlab-CI-CD,docker<!-- /build:tags -->
<!-- build:content -->
## 使用gitlab shared runner

机器不够了，只有用[gitlab shared runner](https://docs.gitlab.com/ee/user/gitlab_com/index.html#linux-shared-runners)了.   
很多时候，我们用`self-hosted runner`无非是需要定制环境或是担心私有代码库的隐私。其实这些是没必要的，因为

- shared runner是以[docker-in-docker](/gitlab-ci-cd-optimize.html)模式执行, 如果需要定制环境可以以`docker:stable`为基础，根据需要构建一个镜像
- 每个shared runner实例一次只运行一个job

> Each instance is used only for one job, this ensures any sensitive data left on the system can’t be accessed by other people their CI jobs.

而dind会保证每次创建的job都是一个干净的环境

### codecov

为了说明上面第一点,这里我想为代码库添加codecov.需要安装curl,bash.   
可以直接在script里面apk add.

```yaml
image: docker:stable
services:
  - docker:dind
...
test:
  stage: test
  script:
    - apk add --update curl bash
    - ci_env=`(curl -s https://codecov.io/env) | bash`
    - docker run $ci_env --env token=$codecov_token --network=host $BUILD_IMAGE_URL /bin/bash -c 'go test -v -race -coverprofile=coverage.txt -covermode=atomic ./... && bash <(curl -s https://codecov.io/bash) -t $token'
  tags:
    - gitlab-org-docker
```

因为是`dind模式`，宿主就是docker:stable.而docker:stable是用alpine作为基础镜像的   
这就是使用docker的好处，如果没有用，就只能折腾`CoreOS`了

### private registry

如果私有仓库域名证书用的是let's encrypt,会报x509: certificate signed by unknown authority.google一下，有很多方法，下面这种最简单

```yaml
image: docker:stable
services:
  - name: docker:dind
    command: ["--insecure-registry=your domain"]
```

当然也可以用gitlab的`container registry`

---
不定期更新
<!-- /build:content -->
