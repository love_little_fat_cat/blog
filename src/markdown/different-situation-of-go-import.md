<!-- build:title -->different situation of go import<!-- /build:title -->
<!-- build:tags -->go<!-- /build:tags -->
<!-- build:content -->
下载未安装的包

```
go get -t -d .
```

从私有仓库下载

`vim ~/.gitconfig`

```
[url "git@gitlab.com:"]
    insteadOf = https://gitlab.com/
```

## no vendor folder

目录

```
├── main.go
└── prefix_pinyin
    └── gen.go
```

### outside gopath

**相对引入**时

```go
import "./prefix_pinyin"
```

可以

```
go run main.go
go build
```

不可以

```
go install
```

因为go install: no install location for directory /home/... outside GOPATH

当然不可以**绝对引入**

```go
import "project/prefix_pinyin"
```

都没有在GOPATH路径下

### inside gopath

**相对引入**时

```go
import "./prefix_pinyin"
```

可以

```
go run main.go
```

不可以

```
go build
go install
```

因为local import "./prefix_pinyin" in non-local package

**绝对引入**当然是没有任何问题的

## vendor folder

go 1.6+

目录

```
├── main.go
├── prefix_pinyin
│   └── gen.go
└── vendor
    └── github.com
        ├── gorilla
        ├── mediocregopher
        └── mozillazg
```

### outside gopath

无论是相对引入还是绝对引入，都不可以

```
go build
go install
go run main.go
```

但是如果本地构建docker镜像,又不想在构建过程中重新`go get`,这时就有用了

- 在GOPATH/src目录下找到需要go get的包，copy到vendor目录
- Dockerfile加入

```
...
COPY . .
RUN go install
...
```

这样就和下面inside gopath的情况一样了

### inside gopath

不可以相对引入

```go
import "./prefix_pinyin"
```

只能绝对引入

```go
import "project/prefix_pinyin"
```

<!-- /build:content -->
