<!-- build:title -->nghttpx tips<!-- /build:title -->
<!-- build:tags --><!-- /build:tags -->
<!-- build:content -->
[nghttpx](https://nghttp2.org/documentation/nghttpx.1.html)是[nghttp2](https://github.com/nghttp2/nghttp2)里的工具，用途嘛，你懂得，哈哈

## 转发https

理论上可以这样配置

```bash
frontend=*,443;no-tls
backend=127.0.0.1,4422;theviper24.today/;tls
```

但是nghttpx是不能转发https的,按作者的[说法](https://github.com/nghttp2/nghttp2/issues/1087)

> nghttpx cannot do passthrough TLS connection. It always terminates TLS, and forwards requests in L7.   
nghttpx is designed as pure L7 proxy

## SNI

```bash
frontend=*,443;tls
backend=127.0.0.1,4143;domain a;no-tls;proto=http/1.1
backend=127.0.0.1,1027;domain b;no-tls;proto=http/1.1

private-key-file=domain a.key
certificate-file=domain a.cer
subcert=domain b.key:domain b.cer
```

按照上面配置的话，会出现`No catch-all backend address is configured`,[文档](https://nghttp2.org/documentation/nghttpx.1.html#connections)上说

> If <PATTERN> is omitted or empty string, “/” is used as pattern, which matches all request paths (catch-all pattern). The catch-all backend must be given.

所以还要加上

```bash
backend=127.0.0.1,1028;;no-tls;proto=http/1.1
```

## http2 proxy

上面的配置是无法根据不同的域名,做相应端口的代理的，也就是说，所有的代理请求都会进入到最后的`catch-all backend`,上面的1028端口   
需要在[frontend](https://nghttp2.org/documentation/nghttpx.1.html#cmdoption-nghttpx-f)加上`sni-fwd`参数,参见[issue](https://github.com/nghttp2/nghttp2/issues/733)

## PROXY protocol

如果上层(nginx,haproxy..)转发用的是[PROXY protocol](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt),需要在[frontend](https://nghttp2.org/documentation/nghttpx.1.html#cmdoption-nghttpx-f)加上`proxyproto`参数,否则无法握手成功

## backend-connections-per-host

之前科学上网,网页多开了点的时候，会出现网页刷不出来的情况,修改[backend-connections-per-host](https://nghttp2.org/documentation/nghttpx.1.html#cmdoption-nghttpx--backend-connections-per-host)就行了，参见[issue](https://github.com/nghttp2/nghttp2/issues/703)

## url forward

nghttpx url转发时，不会去掉url匹配的部分.比如，我想`grafana`通过`my-domain/grafana/`可以访问，nginx的话只需

```nginx
http{
    ...
    server{
        server_name my-domain;
        location /grafana/ {
            proxy_pass http://127.0.0.1:<grafana-port>/;
        }
    }
}
```

请求`my-domain/grafana/login`反向代理到grafana,就变成`127.0.0.1:<grafana-port>/login`了

而nghttpx的话,配置了

```
frontend=0.0.0.0,10001;tls
backend=127.0.0.1,<grafana-port>;/grafana/;proto=http/1.1;no-tls
```

反向代理到grafana,还是原来的`my-domain/grafana/login`,导致404

随便说一下，grafana配置里的`root_url`,只是为后续所有grafana请求'添加前缀',比如，`/public/build/grafana.dark.css`,像下面配置了之后

```
[server]
root_url = %(protocol)s://%(domain)s/grafana/
```

grafana页面会发出`/grafana/public/build/grafana.dark.css`请求，**不表示**可以通过`/grafana/public/build/grafana.dark.css`访问

可以通过nghttpx提供的[mruby extension API](https://nghttp2.org/documentation/nghttpx.1.html#mruby-scripting)重写url

---
不定期更新
<!-- /build:content -->
