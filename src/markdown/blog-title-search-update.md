<!-- build:title -->博客标题搜索-更新<!-- /build:title -->
<!-- build:tags -->algorithm,go,blog<!-- /build:tags -->
<!-- build:content -->

[博客开始支持标题搜索](/blog-title-search.html)中说到两种方式(redis sorted set和trie)保存前缀，并提供前缀搜索.那么如果要更新,就只有删除之前保存的所有数据

## redis sorted set

- 将title和path的映射全部取出
- 对每个title调用Convert方法,生成前缀slice
- 对slice里每个前缀`ZREM`，从sorted set移除
- 删除title和path的映射

```go
func deleteZset() {
    ...
	resp := redisClient.Cmd("HGETALL", "title_path")
	elems, _ := resp.Array()

	var title_path_ikeys []interface{} = make([]interface{}, len(elems)+1)
	title_path_ikeys[0] = "title_path"

	for i, el := range elems {
		title, _ := el.Str()
		title_path_ikeys[i+1] = title
        prefixList := Convert(title)
        for _, key := range prefixList {
            redisClient.PipeAppend("ZREM", key, title)
        }
	}

	redisClient.PipeAppend("HDEL", title_path_ikeys...)
	redisClient.PipeResp()
}
```

## trie

删除过程类似于`dfs`的后序遍历,删除hash中所有指向的节点后，删除该节点

```go
func (t *Trie) DeleteAll(root int64) {
	conn := t.Pool.Get()
	defer conn.Close()

	var dfs func(head int64)
	dfs = func(head int64) {
		keys := []string{}
		i := 0
		resp, _ := R.ByteSlices(conn.Do("HGETALL", head))
		for i < len(resp) {
			key := resp[i]
			val, _ := strconv.Atoi(string(resp[i+1]))
			//key=="end"
			if bytes.Compare(key, []byte{101, 110, 100}) != 0 {
				keys = append(keys, string(key))
				dfs(int64(val))
			}
			i += 2
		}

		for _, d := range keys {
			conn.Send("HDEL", head, d)
		}
	}
	dfs(root)
	conn.Flush()
	conn.Do("DEL", "root")
}
```

---
不定期更新
<!-- /build:content -->
