<!-- build:title -->监控迁移到kubernetes<!-- /build:title -->
<!-- build:tags -->kubernetes,grafana,prometheus<!-- /build:tags -->
<!-- build:content -->
## volumeMounts权限

修改kubernetes yaml配置,执行`kubectl apply -f`,会出现**err="opening storage failed: lock DB directory: open /data/lock: permission denied"**,这就需要配置`initContainers`,在启动真正的image前，修改文件夹权限

```yaml
spec:
  initContainers:
    - name: "init-chown-data"
      image: "busybox:latest"
      imagePullPolicy: "IfNotPresent"
      command: ["chown", "-R", "65534:65534", "/data"]
      volumeMounts:
        - name: data-volume
          mountPath: /data
          subPath: ""
  containers:
    - name: prometheus
      ...
      volumeMounts:
        - name: data-volume
          mountPath: /data
```

## migrate grafana data

`kubectl apply -f`或迁移grafana数据(grafana.db),会出现

```
GF_PATHS_DATA='/var/lib/grafana' is not writable.
You may have issues with file permissions, more information here: http://docs.grafana.org/installation/docker/#migration-from-a-previous-version-of-the-docker-container-to-5-1-or-later
mkdir: cannot create directory '/var/lib/grafana/plugins': Permission denied
```

一样是权限问题.注意这里的userid和groupid要改为**472(grafana)**

```yaml
spec:
  initContainers:
    - name: "init-chown-data"
      image: "busybox:latest"
      imagePullPolicy: "IfNotPresent"
      command: ["chown", "-R", "472:472", "/var/lib/grafana"]
      volumeMounts:
        - name: data-volume
          mountPath: /var/lib/grafana
          subPath: ""
```

可以参考[Installing using Docker](https://docs.grafana.org/installation/docker)

## hostPort and hostNetwork

`hostPort`和`hostNetwork`类似,都可以让我们在宿主机上使用`<host-ip>:<port>`访问服务   
同时在kubernetes上的其他服务**仍然可以**通过`<service-name>:<port>`访问服务,不过如果配置了`hostNetwork:true`,宿主机上可以看到端口号,进程;配置`hostPort:true`则不能

## node exporter监控宿主机

如果要[node-exporter](https://github.com/prometheus/node_exporter)监控**宿主机网络**,需要配置`hostNetwork:true`,不能配置`hostPort:true`   
配置`hostNetwork:true`可以让pod"看到"宿主机的所有网络接口,而配置`hostPort:true`类似[docker networks](/docker-networks.html)中说的，通过iptables规则映射宿主机端口和kubernetes内容器端口，只是**暴露(expose)**端口,pod并没有"挂在"宿主机上,所以看不到端口号，进程,这时如果用node-exporter监控，看到的是pod的网络接口(lo,eth0)

## DaemonSet update strategy

node-exporter适合使用DaemonSet作为控制器(Controller),不过需要注意

- DaemonSet的[更新策略](https://kubernetes.io/docs/tasks/manage-daemon/update-daemon-set/)必须要**显式**设置

```yaml
spec:
  updateStrategy:
    type: OnDelete
```

> To enable the rolling update feature of a DaemonSet, you must set its .spec.updateStrategy.type to RollingUpdate

- 更新策略如果设置的是`OnDelete`,在更新时需要**手动**删除旧的DaemonSet

> OnDelete: With OnDelete update strategy, after you update a DaemonSet template, new DaemonSet pods will only be created when you manually delete old DaemonSet pods.   

- 如果设置的是`RollingUpdate`,这个rolling update的行为和其他控制器(如Deployment)**不一样**,`DaemonSet`的rolling update是直接删除旧的DaemonSet,然后创建新的DaemonSet

> With RollingUpdate update strategy, after you update a DaemonSet template, old DaemonSet pods will be killed, and new DaemonSet pods will be created automatically, in a controlled fashion.   

---
不定期更新
<!-- /build:content -->
