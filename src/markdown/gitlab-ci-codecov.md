<!-- build:title -->gitlab CI中使用codecov<!-- /build:title -->
<!-- build:tags -->gitlab-CI-CD,docker<!-- /build:tags -->
<!-- build:content -->

若是使用`shell executor`,和[travis ci](https://github.com/RandomCivil/http2-proxy/blob/master/.travis.yml)中使用类似

```yaml
stages:
  - test

test:
  stage: test
  script:
    - CGO_ENABLED=0 go test -v -coverprofile=cover.txt -covermode=atomic ./...
    - bash <(curl -s https://codecov.io/bash) -t $codecov_token -f cover.txt
  tags:
    - shell
```

gitlab ci中能使用`docker executor`就尽量使用,codecov有相应[文档](https://docs.codecov.io/docs/testing-with-docker)

## Codecov Inside Docker

[.gitlab-ci.yml](https://gitlab.com/love_little_fat_cat/ci-codecov/blob/7c365f3e0eebca4a3f4dae3e0cb22fe664dcc65b/.gitlab-ci.yml)

```yaml
image: docker:stable

stages:
  - build
  - test

variables:
  CACHE_IMAGE_URL: theviper/ci-codecov:cache
  BUILD_IMAGE_URL: theviper/ci-codecov:$CI_COMMIT_SHA

....

test:
  stage: test
  script:
    - docker pull $CACHE_IMAGE_URL || true
    - apk add --no-cache bash curl
    - ci_env=`(curl -s https://codecov.io/env) | bash`
    - docker run $ci_env --env token=$codecov_token --env GO111MODULE=on --network=host $CACHE_IMAGE_URL /bin/bash -c 'go test -v -coverprofile=coverage.txt -covermode=atomic ./... && bash <(curl -s https://codecov.io/bash) -t $token'
  only:
    - merge_requests
    - master
  tags:
    - docker
```

### docker socket bind

- `ci_env`不能像文档所说，写成

```
ci_env=`bash <(curl -s https://codecov.io/env)`
```

会报错/bin/sh: eval: line 1: syntax error: unexpected "(",见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346791204),因此需要换一种写法

```
ci_env=`(curl -s https://codecov.io/env) | bash`
```

- 设置`ci_env`时用到bash,curl,需要在设置前安装
- docker run需要设置`--network=host`,否则`main_test.go`中localhost无效
- docker run需要加上/bin/bash -c,否则报错,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346799969)
- gitlab ci设置的`Variables`,需要通过`--env`传给CACHE_IMAGE_URL,因为直接

```bash
go test -v -coverprofile=coverage.txt -covermode=atomic ./... && bash <(curl -s https://codecov.io/bash) -t $codecov_token
```

会报

```
HTTP 400
Please provide the repository token to upload reports via `-t :repository-token`
```

说明设置的`Variables`没有传进去

- `-coverprofile`输出的文件名**必须是coverage.txt**.如果是其他文件名，即使codecov加了`-f cover.txt`也没用,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346833045)

### dind

这种方式也可以与codecov集成,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346788719)

## Codecov Outside Docker

[.gitlab-ci.yml](https://gitlab.com/love_little_fat_cat/ci-codecov/blob/0b51d42c41a030d8c885d0ed899c1e3489024254/.gitlab-ci.yml)

```yaml
....
test:
  stage: test
  script:
    - docker pull $CACHE_IMAGE_URL || true
    - apk add --no-cache bash curl
    - export SHARED_PATH="$(dirname ${CI_PROJECT_DIR})/shared"
    - echo ${SHARED_PATH}
    - mkdir -p ${SHARED_PATH}
    - docker run -v ${SHARED_PATH}:/shared --env GO111MODULE=on --network=host $CACHE_IMAGE_URL /bin/bash -c 'go test -v -coverprofile=coverage.txt -covermode=atomic ./... && mv /go/src/ci-codecov/coverage.txt /shared'
    - find / -name coverage.txt
    - bash -c "bash <(curl -s https://codecov.io/bash) -t $codecov_token -f ${SHARED_PATH}/coverage.txt"
```

### dind

- docker run需要加上/bin/bash -c,否则报错,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346898478)
- 不能像上面一样

```bash
bash <(curl -s https://codecov.io/bash) -t $codecov_token
```

会报错/bin/sh: eval: line 102: syntax error: unexpected "(",见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346928469),需要加上bash -c

```
bash -c "bash <(curl -s https://codecov.io/bash) -t $codecov_token"
```

- 最后一行执行codecov脚本，需要加上`-f`参数,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/346971417)

### docker socket bind


这种方式也可以与codecov集成,见[job](https://gitlab.com/love_little_fat_cat/ci-codecov/-/jobs/347235553)

<!-- /build:content -->
